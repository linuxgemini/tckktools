# TCKKTools

Random tools to interface with the Turkish ID card.

Most of these are quick scripts I made a while back but am now choosing to publish.

### List of tools

- `dumpcert.py` -> Dumps the ID Card certificate (Not the key! This can only be used to verify identity and ID validity.)
- `verifypin.py` -> Verifies the ID Card PIN (User needs to supply the PIN on run)

### Based? Based on what?

Work here is based on the following Turkish Standards: TS 13582, TS 13583, TS 13584, TS 13585, TS 13678, TS 13679, TS 13680, TS 13681.

Similarly, ISO/IEC 7816-4 was consulted too.

