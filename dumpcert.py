from smartcard.System import readers
import array

# Connect to the card
r = readers()
connection = r[0].createConnection()
connection.connect()

# Select "MF" application (technically 0x63 0x00)
resp = connection.transmit([0, 0xA4, 0, 0, 0])
# Select "KDU" (0x3D 0x00) under "MF"
resp = connection.transmit([0, 0xA4, 0, 0, 0x02, 0x3D, 0, 0])
# Get length of "FID_PKCS15_CERT1" (0x2F 0x10)
resp = connection.transmit([0, 0xA4, 0, 0, 0x02, 0x2F, 0x10, 0])
# print(resp[0])
bytes_to_read = (resp[0][4] << 8) + resp[0][5]
print(f"Bytes to read: {bytes_to_read}")

# Start reading the file
bytes_read = 0
cert_barray = []
while bytes_read < bytes_to_read:  # Maximum is ~0xFE00 (0xFD80?)
    # Determine how much we read so far so that we can seek
    read_b1 = bytes_read % (0xFF + 1)
    read_b2 = int(bytes_read / 0xFE)

    # Determine how much to read (we can read at most 0xD0 at a time)
    left_to_read = bytes_to_read - bytes_read
    left_to_read = 0xD0 if left_to_read > 0xD0 else left_to_read

    # Send read file command and get response
    resp = connection.transmit([0, 0xB0, read_b2, read_b1, 0xD0])

    # Save cert's bytes and update read byte count
    bytes_read += len(resp[0])
    cert_barray += resp[0]

    # if for some reason we read too much, the card will only send
    # as much as it has, in such a case stop reading further.
    if len(resp[0]) != left_to_read:
        print(f"No more data, read {len(resp[0])} instead of {left_to_read}.")
        break
print(f"Read {bytes_read} bytes.")

# Convert the int-byte array to proper bytes, and write it to a file
cert_bytes = array.array('B', cert_barray).tobytes()
with open("cert.der", "wb") as f:
    f.write(cert_bytes)

